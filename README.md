# Notebooks Toponym Geocoding

This repository contains different notebooks to display the results obtained by the approach proprosed in https://gitlab.liris.cnrs.fr/jfize/toponym-geocoding.

You can directly execute each notebook by using the Binder service [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.liris.cnrs.fr%2Fjfize%2Fnotebooks-toponym-geocoding.git/master)

## Authors
Jacques Fize, Ludovic Moncla
